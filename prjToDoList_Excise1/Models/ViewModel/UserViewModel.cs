﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace prjToDoList_Excise1.Models.ViewModel
{
    public class UserViewModel
    {
        public int ID { get; set; }

        [DisplayName("姓名/帳號")]
        [Required]
        public string Name { get; set; }

        [DisplayName("電子郵件")]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("密碼")]
        [Required]
        public string Password { get; set; }
    }
}