﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace prjToDoList_Excise1.Models.ViewModel
{
    public class TodoViewModel
    {
        public int ID { get; set; }

        [DisplayName("內容")]
        [Required]
        public string Content { get; set; }

        [DisplayName("日期")]
        [Required]
        public System.DateTime UpdatedAt { get; set; }

        [DisplayName("是否完成")]
        [Required]
        public bool Completed { get; set; }

        public int UserID { get; set; }
    }
}