﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace prjToDoList_Excise1.Models.ViewModel
{
    public class RegisterViewModel
    {
        [DisplayName("姓名/帳號")]
        [Required]
        public string Name { get; set; }

        [DisplayName("電子郵件")]
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("密碼")]
        [Required]
        public string Password { get; set; }

        [DisplayName("確認密碼")]
        [Required]
        [Compare("Password")]
        public string ConfirmPwd { get; set; }
    }
}