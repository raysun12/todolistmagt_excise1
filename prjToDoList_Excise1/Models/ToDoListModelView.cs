﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prjToDoList_Excise1.Models
{
    public class ToDoListModelView
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string UpdatedAt { get; set; }
        public bool Completed { get; set; }
    }
}