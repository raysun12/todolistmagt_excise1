﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using prjToDoList_Excise1.Models.ViewModel;
using prjToDoList_Excise1.Models;
using System.Web.Security;
using System.Web.Helpers;

namespace prjToDoList_Excise1.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        TestDBEntities db = new TestDBEntities();

        // GET: Home
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Index(UserViewModel user)
        {
            if (ModelState.IsValid == false) return View();

            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(user.Password));
            user.Password = Convert.ToBase64String(s);

            var result = db.Users
                           .Where(m => m.Name == user.Name && m.Password == user.Password)
                           .FirstOrDefault();

            if (result == null)
            {
                ViewBag.Message = "帳號或密碼錯誤";
                return View();
            }                        

            Session["User"] = result;
            Session["WelCome"] = result.Name + "的代辦清單";

            //set cookie will be expired after 30 min(default) set in web.config file
            FormsAuthentication.SetAuthCookie(user.Name, true);

            return RedirectToAction("TodoList");
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            Session.Clear();

            //should be deleted validation after logout
            FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid == false) return View("Register");

            var result = db.Users.Where(m => m.Name == registerViewModel.Name).FirstOrDefault();
            if (result != null)
            {
                ViewBag.Message = "此帳號已有人註冊";
                return View("Register");
            }

            Users user = new Users();
            user.Name = registerViewModel.Name;
            user.Email = registerViewModel.Email;

            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(registerViewModel.Password));
            user.Password = Convert.ToBase64String(s);

            db.Users.Add(user);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult TodoList()
        {
            return View("TodoList", "_LayoutLogin");
        }

        [HttpPost]
        public ActionResult TodoListApi(int curPage = 1)
        {
            const int itemPerPage = 5;
            List<ToDoListModelView> toDoListModelViews = new List<ToDoListModelView>();

            var id = (Session["User"] as Users).ID;

            var totalItems = db.Todos
                               .Where(m => m.UserID == id)
                               .Count();

            if (totalItems <= 0) return Json( new { data = "none" });

            var totalPages = ((totalItems - 1) / itemPerPage) + 1;
            if (totalPages <= 0) totalPages = 1;

            var todos = db.Todos
                          .Where(m => m.UserID == id)
                          .OrderBy(m => m.ID)
                          .Skip(itemPerPage * (curPage - 1))
                          .Take(itemPerPage);                      

            foreach(var item in todos)
            {
                ToDoListModelView toDoListModelView = new ToDoListModelView();
                toDoListModelView.ID = item.ID;
                toDoListModelView.Content = item.Content;
                toDoListModelView.UpdatedAt = item.UpdatedAt.ToString("yyyy-MM-dd HH:mm:ss"); ;
                toDoListModelView.Completed = item.Completed;
                toDoListModelViews.Add(toDoListModelView);
            }

            return Json( new { totalPages = totalPages, curPage = curPage, curItem = ((curPage - 1) * 5) + 1, totalItems = totalItems, toDoListModelViews });
        }

        [HttpPost]
        public ActionResult GetConentApi(int id)
        {
            var todo = db.Todos
                         .Where(m => m.ID == id)
                         .FirstOrDefault();

            ToDoListModelView toDoListModelView = new ToDoListModelView();
            toDoListModelView.ID = todo.ID;
            toDoListModelView.Content = todo.Content;
            toDoListModelView.UpdatedAt = todo.UpdatedAt.ToString("yyyy-MM-ddTHH:mm:ss"); ;
            toDoListModelView.Completed = todo.Completed;

            return Json(toDoListModelView);
        }

        [HttpPost]
        public int DelItemApi(int id)
        {
            int num = 0;

            try
            {
                var todo = db.Todos.Where(m => m.ID == id).FirstOrDefault();
                db.Todos.Remove(todo);
                num = db.SaveChanges();
            }
            catch (Exception ex)
            {
                num = 0;
            }

            return num;            
        }

        [HttpPost]
        public int CreateItemApi(string content, string updatedAt, Boolean completed)
        {
            int num = 0;

            try
            {
                DateTime dt = Convert.ToDateTime(updatedAt);

                Todos todo = new Todos();
                todo.Content = content;
                todo.UpdatedAt = dt;
                todo.Completed = completed;
                todo.UserID = (Session["User"] as Users).ID;

                db.Todos.Add(todo);
                num = db.SaveChanges();
            }
            catch
            {
                num = 0;
            }

            return num;
        }

        [HttpPost]
        public int EditItemApi(int id, string content, string updatedAt, Boolean completed)
        {
            int num = 0;

            try { 

                var todo = db.Todos.Where(m => m.ID == id).FirstOrDefault();

                DateTime dt = Convert.ToDateTime(updatedAt);

                todo.Content = content;
                todo.UpdatedAt = dt;
                todo.Completed = completed;

                num = db.SaveChanges();
            }
            catch (Exception ex)
            {
                num = 0;
            }

            return num;
        }
    }
}